package View;

import Interface.Tributavel;
import Models.ContaCorrente;

public class TestaTributavel {
	
	public static void main(String[] args){
		
		ContaCorrente contaCorrente = new ContaCorrente();
		contaCorrente.deposita(100);
		
		System.out.println(contaCorrente.calculaTributos());
		
		Tributavel t = contaCorrente;
		
		System.out.println(t.calculaTributos());
		
	}

}
