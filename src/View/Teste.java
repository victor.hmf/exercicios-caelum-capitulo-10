package View;

import Interface.AreaCalculavel;
import Models.Quadrado;
import Models.Retangulo;

public class Teste {

	public static void main(String[] args){
		
		AreaCalculavel retangulo = new Retangulo(3,2);
		System.out.println("A área do Retângulo é: " + retangulo.calculaArea());
		
		AreaCalculavel quadrado = new Quadrado(5);
		System.out.println("A área do Quadrado é: " + quadrado.calculaArea());
		
		
	}
}
